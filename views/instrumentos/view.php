<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Instrumentos */

$this->title = $model->cod_conciertos;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="instrumentos-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_conciertos',
            'instrumentos',
        ],
    ]) ?>

</div>
