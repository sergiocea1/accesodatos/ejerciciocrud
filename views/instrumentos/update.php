<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instrumentos */

$this->title = 'Update Instrumentos: ' . $model->cod_conciertos;
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_conciertos, 'url' => ['view', 'cod_conciertos' => $model->cod_conciertos, 'instrumentos' => $model->instrumentos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="instrumentos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
