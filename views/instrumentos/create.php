<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Instrumentos */

$this->title = 'Create Instrumentos';
$this->params['breadcrumbs'][] = ['label' => 'Instrumentos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="instrumentos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
