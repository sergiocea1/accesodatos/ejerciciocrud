<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */

$this->title = 'Update Compran: ' . $model->cod_clientes;
$this->params['breadcrumbs'][] = ['label' => 'Comprans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_clientes, 'url' => ['view', 'cod_clientes' => $model->cod_clientes, 'cod_entradas' => $model->cod_entradas, 'cod_festivales' => $model->cod_festivales]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="compran-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
