<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Compran */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="compran-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_clientes')->textInput() ?>

    <?= $form->field($model, 'cod_entradas')->textInput() ?>

    <?= $form->field($model, 'cod_festivales')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
