<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conciertos */

$this->title = 'Create Conciertos';
$this->params['breadcrumbs'][] = ['label' => 'Conciertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conciertos-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
