<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Conciertos */

$this->title = 'Update Conciertos: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Conciertos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="conciertos-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
