<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Conciertos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="conciertos-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Create Conciertos', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'cod',
            'fecha',
            'hora_inicio',
            'hora_fin',
            'cod_festivales',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>
