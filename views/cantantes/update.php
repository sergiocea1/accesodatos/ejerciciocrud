<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantantes */

$this->title = 'Update Cantantes: ' . $model->cod;
$this->params['breadcrumbs'][] = ['label' => 'Cantantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod, 'url' => ['view', 'id' => $model->cod]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cantantes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
