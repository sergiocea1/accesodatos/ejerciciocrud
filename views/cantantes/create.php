<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantantes */

$this->title = 'Create Cantantes';
$this->params['breadcrumbs'][] = ['label' => 'Cantantes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cantantes-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
