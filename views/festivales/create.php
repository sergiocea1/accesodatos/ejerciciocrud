<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Festivales */

$this->title = 'Create Festivales';
$this->params['breadcrumbs'][] = ['label' => 'Festivales', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="festivales-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
