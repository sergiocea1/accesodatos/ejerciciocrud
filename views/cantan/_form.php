<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cantan-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'cod_cantantes')->textInput() ?>

    <?= $form->field($model, 'cod_conciertos')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
