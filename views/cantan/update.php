<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */

$this->title = 'Update Cantan: ' . $model->cod_cantantes;
$this->params['breadcrumbs'][] = ['label' => 'Cantans', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->cod_cantantes, 'url' => ['view', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="cantan-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
