<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */

$this->title = 'Create Cantan';
$this->params['breadcrumbs'][] = ['label' => 'Cantans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="cantan-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
