<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model app\models\Cantan */

$this->title = $model->cod_cantantes;
$this->params['breadcrumbs'][] = ['label' => 'Cantans', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
\yii\web\YiiAsset::register($this);
?>
<div class="cantan-view">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Update', ['update', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos], ['class' => 'btn btn-primary']) ?>
        <?= Html::a('Delete', ['delete', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'cod_cantantes',
            'cod_conciertos',
        ],
    ]) ?>

</div>
