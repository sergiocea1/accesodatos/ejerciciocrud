<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cantan".
 *
 * @property int $cod_cantantes
 * @property int $cod_conciertos
 *
 * @property Cantantes $codCantantes
 * @property Conciertos $codConciertos
 */
class Cantan extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cantan';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_cantantes', 'cod_conciertos'], 'required'],
            [['cod_cantantes', 'cod_conciertos'], 'integer'],
            [['cod_cantantes', 'cod_conciertos'], 'unique', 'targetAttribute' => ['cod_cantantes', 'cod_conciertos']],
            [['cod_cantantes'], 'exist', 'skipOnError' => true, 'targetClass' => Cantantes::className(), 'targetAttribute' => ['cod_cantantes' => 'cod']],
            [['cod_conciertos'], 'exist', 'skipOnError' => true, 'targetClass' => Conciertos::className(), 'targetAttribute' => ['cod_conciertos' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_cantantes' => 'Cod Cantantes',
            'cod_conciertos' => 'Cod Conciertos',
        ];
    }

    /**
     * Gets query for [[CodCantantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCantantes()
    {
        return $this->hasOne(Cantantes::className(), ['cod' => 'cod_cantantes']);
    }

    /**
     * Gets query for [[CodConciertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodConciertos()
    {
        return $this->hasOne(Conciertos::className(), ['cod' => 'cod_conciertos']);
    }
}
