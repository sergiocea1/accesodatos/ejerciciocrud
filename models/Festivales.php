<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "festivales".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $fecha_inicio
 * @property string|null $fecha_fin
 *
 * @property Conciertos[] $conciertos
 * @property Entradas[] $entradas
 */
class Festivales extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'festivales';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha_inicio', 'fecha_fin'], 'safe'],
            [['nombre'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'fecha_inicio' => 'Fecha Inicio',
            'fecha_fin' => 'Fecha Fin',
        ];
    }

    /**
     * Gets query for [[Conciertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getConciertos()
    {
        return $this->hasMany(Conciertos::className(), ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[Entradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getEntradas()
    {
        return $this->hasMany(Entradas::className(), ['cod_festivales' => 'cod']);
    }
}
