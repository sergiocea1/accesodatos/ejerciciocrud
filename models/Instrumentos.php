<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "instrumentos".
 *
 * @property int $cod_conciertos
 * @property string $instrumentos
 *
 * @property Conciertos $codConciertos
 */
class Instrumentos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'instrumentos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_conciertos', 'instrumentos'], 'required'],
            [['cod_conciertos'], 'integer'],
            [['instrumentos'], 'string', 'max' => 25],
            [['cod_conciertos', 'instrumentos'], 'unique', 'targetAttribute' => ['cod_conciertos', 'instrumentos']],
            [['cod_conciertos'], 'exist', 'skipOnError' => true, 'targetClass' => Conciertos::className(), 'targetAttribute' => ['cod_conciertos' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_conciertos' => 'Cod Conciertos',
            'instrumentos' => 'Instrumentos',
        ];
    }

    /**
     * Gets query for [[CodConciertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodConciertos()
    {
        return $this->hasOne(Conciertos::className(), ['cod' => 'cod_conciertos']);
    }
}
