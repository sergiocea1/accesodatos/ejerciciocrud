<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "entradas".
 *
 * @property int $cod
 * @property int|null $numero_entrada
 * @property int|null $cod_festivales
 *
 * @property Compran[] $comprans
 * @property Clientes[] $codClientes
 * @property Clientes[] $codFestivales
 * @property Festivales $codFestivales0
 */
class Entradas extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'entradas';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod'], 'required'],
            [['cod', 'numero_entrada', 'cod_festivales'], 'integer'],
            [['cod'], 'unique'],
            [['cod_festivales'], 'exist', 'skipOnError' => true, 'targetClass' => Festivales::className(), 'targetAttribute' => ['cod_festivales' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'numero_entrada' => 'Numero Entrada',
            'cod_festivales' => 'Cod Festivales',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::className(), ['cod_entradas' => 'cod']);
    }

    /**
     * Gets query for [[CodClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodClientes()
    {
        return $this->hasMany(Clientes::className(), ['cod' => 'cod_clientes'])->viaTable('compran', ['cod_entradas' => 'cod']);
    }

    /**
     * Gets query for [[CodFestivales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFestivales()
    {
        return $this->hasMany(Clientes::className(), ['cod' => 'cod_festivales'])->viaTable('compran', ['cod_entradas' => 'cod']);
    }

    /**
     * Gets query for [[CodFestivales0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFestivales0()
    {
        return $this->hasOne(Festivales::className(), ['cod' => 'cod_festivales']);
    }
}
