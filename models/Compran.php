<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "compran".
 *
 * @property int $cod_clientes
 * @property int $cod_entradas
 * @property int $cod_festivales
 *
 * @property Clientes $codClientes
 * @property Entradas $codEntradas
 * @property Clientes $codFestivales
 */
class Compran extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'compran';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['cod_clientes', 'cod_entradas', 'cod_festivales'], 'required'],
            [['cod_clientes', 'cod_entradas', 'cod_festivales'], 'integer'],
            [['cod_clientes', 'cod_entradas'], 'unique', 'targetAttribute' => ['cod_clientes', 'cod_entradas']],
            [['cod_clientes', 'cod_festivales'], 'unique', 'targetAttribute' => ['cod_clientes', 'cod_festivales']],
            [['cod_entradas', 'cod_festivales'], 'unique', 'targetAttribute' => ['cod_entradas', 'cod_festivales']],
            [['cod_clientes', 'cod_entradas', 'cod_festivales'], 'unique', 'targetAttribute' => ['cod_clientes', 'cod_entradas', 'cod_festivales']],
            [['cod_clientes'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cod_clientes' => 'cod']],
            [['cod_entradas'], 'exist', 'skipOnError' => true, 'targetClass' => Entradas::className(), 'targetAttribute' => ['cod_entradas' => 'cod']],
            [['cod_festivales'], 'exist', 'skipOnError' => true, 'targetClass' => Clientes::className(), 'targetAttribute' => ['cod_festivales' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod_clientes' => 'Cod Clientes',
            'cod_entradas' => 'Cod Entradas',
            'cod_festivales' => 'Cod Festivales',
        ];
    }

    /**
     * Gets query for [[CodClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodClientes()
    {
        return $this->hasOne(Clientes::className(), ['cod' => 'cod_clientes']);
    }

    /**
     * Gets query for [[CodEntradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntradas()
    {
        return $this->hasOne(Entradas::className(), ['cod' => 'cod_entradas']);
    }

    /**
     * Gets query for [[CodFestivales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFestivales()
    {
        return $this->hasOne(Clientes::className(), ['cod' => 'cod_festivales']);
    }
}
