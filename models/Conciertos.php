<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "conciertos".
 *
 * @property int $cod
 * @property string|null $fecha
 * @property string|null $hora_inicio
 * @property string|null $hora_fin
 * @property int|null $cod_festivales
 *
 * @property Cantan[] $cantans
 * @property Cantantes[] $codCantantes
 * @property Festivales $codFestivales
 * @property Instrumentos[] $instrumentos
 */
class Conciertos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'conciertos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['fecha', 'hora_inicio', 'hora_fin'], 'safe'],
            [['cod_festivales'], 'integer'],
            [['cod_festivales'], 'exist', 'skipOnError' => true, 'targetClass' => Festivales::className(), 'targetAttribute' => ['cod_festivales' => 'cod']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'fecha' => 'Fecha',
            'hora_inicio' => 'Hora Inicio',
            'hora_fin' => 'Hora Fin',
            'cod_festivales' => 'Cod Festivales',
        ];
    }

    /**
     * Gets query for [[Cantans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCantans()
    {
        return $this->hasMany(Cantan::className(), ['cod_conciertos' => 'cod']);
    }

    /**
     * Gets query for [[CodCantantes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodCantantes()
    {
        return $this->hasMany(Cantantes::className(), ['cod' => 'cod_cantantes'])->viaTable('cantan', ['cod_conciertos' => 'cod']);
    }

    /**
     * Gets query for [[CodFestivales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFestivales()
    {
        return $this->hasOne(Festivales::className(), ['cod' => 'cod_festivales']);
    }

    /**
     * Gets query for [[Instrumentos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getInstrumentos()
    {
        return $this->hasMany(Instrumentos::className(), ['cod_conciertos' => 'cod']);
    }
}
