<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "cantantes".
 *
 * @property int $cod
 * @property string|null $nombre
 * @property string|null $apellidos
 *
 * @property Cantan[] $cantans
 * @property Conciertos[] $codConciertos
 */
class Cantantes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'cantantes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['nombre', 'apellidos'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
        ];
    }

    /**
     * Gets query for [[Cantans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCantans()
    {
        return $this->hasMany(Cantan::className(), ['cod_cantantes' => 'cod']);
    }

    /**
     * Gets query for [[CodConciertos]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodConciertos()
    {
        return $this->hasMany(Conciertos::className(), ['cod' => 'cod_conciertos'])->viaTable('cantan', ['cod_cantantes' => 'cod']);
    }
}
