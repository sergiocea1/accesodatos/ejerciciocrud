<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "clientes".
 *
 * @property int $cod
 * @property string|null $dni
 * @property string|null $nombre
 * @property string|null $apellidos
 * @property int|null $edad
 * @property string|null $fecha_nacimiento
 *
 * @property Compran[] $comprans
 * @property Compran[] $comprans0
 * @property Entradas[] $codEntradas
 * @property Clientes[] $codFestivales
 * @property Clientes[] $codClientes
 * @property Entradas[] $codEntradas0
 */
class Clientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'clientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['edad'], 'integer'],
            [['fecha_nacimiento'], 'safe'],
            [['dni'], 'string', 'max' => 9],
            [['nombre', 'apellidos'], 'string', 'max' => 25],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'cod' => 'Cod',
            'dni' => 'Dni',
            'nombre' => 'Nombre',
            'apellidos' => 'Apellidos',
            'edad' => 'Edad',
            'fecha_nacimiento' => 'Fecha Nacimiento',
        ];
    }

    /**
     * Gets query for [[Comprans]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans()
    {
        return $this->hasMany(Compran::className(), ['cod_clientes' => 'cod']);
    }

    /**
     * Gets query for [[Comprans0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getComprans0()
    {
        return $this->hasMany(Compran::className(), ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[CodEntradas]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntradas()
    {
        return $this->hasMany(Entradas::className(), ['cod' => 'cod_entradas'])->viaTable('compran', ['cod_clientes' => 'cod']);
    }

    /**
     * Gets query for [[CodFestivales]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodFestivales()
    {
        return $this->hasMany(Clientes::className(), ['cod' => 'cod_festivales'])->viaTable('compran', ['cod_clientes' => 'cod']);
    }

    /**
     * Gets query for [[CodClientes]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodClientes()
    {
        return $this->hasMany(Clientes::className(), ['cod' => 'cod_clientes'])->viaTable('compran', ['cod_festivales' => 'cod']);
    }

    /**
     * Gets query for [[CodEntradas0]].
     *
     * @return \yii\db\ActiveQuery
     */
    public function getCodEntradas0()
    {
        return $this->hasMany(Entradas::className(), ['cod' => 'cod_entradas'])->viaTable('compran', ['cod_festivales' => 'cod']);
    }
}
