<?php

namespace app\controllers;

use Yii;
use app\models\Cantan;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;

/**
 * CantanController implements the CRUD actions for Cantan model.
 */
class CantanController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Cantan models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Cantan::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Cantan model.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($cod_cantantes, $cod_conciertos)
    {
        return $this->render('view', [
            'model' => $this->findModel($cod_cantantes, $cod_conciertos),
        ]);
    }

    /**
     * Creates a new Cantan model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Cantan();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Cantan model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($cod_cantantes, $cod_conciertos)
    {
        $model = $this->findModel($cod_cantantes, $cod_conciertos);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'cod_cantantes' => $model->cod_cantantes, 'cod_conciertos' => $model->cod_conciertos]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Cantan model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($cod_cantantes, $cod_conciertos)
    {
        $this->findModel($cod_cantantes, $cod_conciertos)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Cantan model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $cod_cantantes
     * @param integer $cod_conciertos
     * @return Cantan the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($cod_cantantes, $cod_conciertos)
    {
        if (($model = Cantan::findOne(['cod_cantantes' => $cod_cantantes, 'cod_conciertos' => $cod_conciertos])) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
